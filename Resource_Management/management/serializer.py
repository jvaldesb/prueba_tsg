# usuarios/serializer.py

# Importaciones de Django

# Importaciones de REST
from rest_framework import serializers

# Importaciones propias
from . import models

class Resource_Serializer (serializers.ModelSerializer):
    
    class Meta:
        model = models.Resource
        fields = ('id', 'name', 'serial', 'brand', 'type_resource', 'provider', 'commercial_value', 'date_purchase', 'state_resource')

class Responsable_Serializer (serializers.ModelSerializer):
    
    class Meta:
        model = models.Responsable
        fields = ('id', 'name', 'type_responsable')

class Assignment_Serializer (serializers.ModelSerializer):
    
    class Meta:
        model = models.Assignment
        fields = ('id', 'responsable', 'resource', 'date_assignment')