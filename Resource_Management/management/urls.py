# management/urls.py

# Importaciones de Django

# Importaciones de REST
from rest_framework import routers

# Importaciones propias
from management import views as viewMana

router = routers.DefaultRouter()
router.register(r'resource', viewMana.Resource_ViewSet)
router.register(r'responsable', viewMana.Responsable_ViewSet)
router.register(r'assignment', viewMana.Assignment_ViewSet)
