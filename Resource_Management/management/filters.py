from django_filters import rest_framework as filters
from . import models
from . import serializer

class Resource_FilterSet(filters.FilterSet):
    brand = filters.CharFilter(field_name='brand')

    class Meta:
       model = models.Resource
       fields ={
           'serial': ['exact'], 
           'brand': ['exact', 'contains'], 
           'type_resource': ['exact']
       }

class Responsable_FilterSet(filters.FilterSet):
    name = filters.CharFilter(field_name='name')

    class Meta:
       model = models.Responsable
       fields = {
           'name': ['exact', 'contains']
       } 