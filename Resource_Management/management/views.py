# usuarios/views_api.py

# Importaciones de Django
from django.shortcuts import render, get_object_or_404

# Importaciones de REST
from rest_framework import viewsets, generics, filters
from rest_framework.response import Response

# Importaciones propias
from . import models
from . import serializer
from . import filters

class Resource_ViewSet (viewsets.ModelViewSet):

    queryset = models.Resource.objects.all()
    serializer_class = serializer.Resource_Serializer
    filterset_class = filters.Resource_FilterSet
    

class Responsable_ViewSet (viewsets.ModelViewSet):

    queryset = models.Responsable.objects.all()
    serializer_class = serializer.Responsable_Serializer
    filterset_class = filters.Responsable_FilterSet

class Assignment_ViewSet (viewsets.ModelViewSet):

    queryset = models.Assignment.objects.all()
    serializer_class = serializer.Assignment_Serializer