# management/models.py

# Importaciones de DJango
from django.db import models

# Importaciones propias

class Resource (models.Model):
    TECHNOLOGICAL = 'TEC'
    OFFICE = 'OFI'
    CAFETERIA = 'CAF'
    TYPE_CHOICES = [
        (TECHNOLOGICAL, 'Tecnológico'),
        (OFFICE, 'Oficina'),
        (CAFETERIA, 'Cafetería'),
    ]
    GOOD = 'BE'
    BAD = 'ME'
    NEW = 'NU'
    STATE_CHOICES = [
        (GOOD, 'Buen Estado'),
        (BAD, 'Mal Estado'),
        (NEW, 'Nuevo'),
    ]
    name = models.CharField(verbose_name='Nombre Producto', max_length=400, null=False)
    serial = models.CharField(verbose_name='Serial', max_length=70, null=False)
    brand = models.CharField(verbose_name='Marca', max_length=150, null=False)
    type_resource = models.CharField(max_length=3, choices=TYPE_CHOICES, verbose_name='Tipo de Recurso')
    provider = models.CharField(verbose_name='Proveedor', max_length=300, null=False)
    commercial_value = models.IntegerField(verbose_name='Valor Comercial')
    date_purchase = models.DateField(auto_now=False, auto_now_add=False, verbose_name='Fecha de Compra')
    state_resource = models.CharField(max_length=3, choices=STATE_CHOICES, verbose_name='Estado del Recurso')



    class Meta:
        verbose_name ='Recurso'
        verbose_name_plural = 'Recursos'

    def __str__(self):
        return str(self.id)+" ["+self.serial+"] "+self.brand

class Responsable (models.Model):
    PERSON = 'PE'
    AREA = 'AR'
    TYPE_CHOICES = [
        (PERSON, 'Persona'),
        (AREA, 'Área'),
    ]
    name = models.CharField(verbose_name='Nombre Completo', max_length=400, null=False)
    type_responsable = models.CharField(max_length=2, choices=TYPE_CHOICES, verbose_name='Tipo de Responsable')

    class Meta:
        verbose_name ='Responsable'
        verbose_name_plural = 'Responsables'

    def __str__(self):
        return str(self.id)+" ["+self.type_responsable+"] "+self.name

class Assignment (models.Model):
    responsable = models.ForeignKey(Responsable, verbose_name='Responsable', on_delete=models.CASCADE)
    resource = models.ForeignKey(Resource, verbose_name='Responsable', on_delete=models.CASCADE)
    date_assignment = models.DateField(auto_now=False, auto_now_add=False, verbose_name='Fecha de Asignación')

    class Meta:
        verbose_name ='Asignación'
        verbose_name_plural = 'Asignaciones'

    def __str__(self):
        return str(self.id)+" ["+self.responsable.name+"] ["+self.resource.serial+" "+self.resource.brand+"]"


